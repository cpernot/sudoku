Array::remove = (item) ->
        i = @indexOf(item)
        @splice(i,1) if i >= 0
        return

sleep = (ms) ->
        new Promise (resolve) ->
                window.setTimeout(resolve,ms)

tick = 0

class SudokuGrid
        constructor: (@status, @table, @depth) ->
 
        get: (x,y)->
                @status[x+y*9]
        set: (x,y,s) ->
                @status[x+y*9] = s
                # @table.children[y].children[x].children[0].value = s
                return
        render: ->
                for x in [0..8]
                        for y in [0..8]
                                @table.children[y].children[x].children[0].value = "#{@get(x,y)}"
                return

        possibles: (x,y) ->
                results = [1..9]
                region_x = x//3
                region_y = y//3

                for neigh_x in [region_x*3..region_x*3+2]
                        for neigh_y in [region_y*3..region_y*3+2]
                                if neigh_x != x and neigh_y != y
                                        results.remove(@get(neigh_x,neigh_y))
                for line_x in [0..8]
                        if line_x != x
                                results.remove(@get(line_x,y))
                for line_y in [0..8]
                        if line_y != y
                                results.remove(@get(x,line_y))
                return results

        solve: ->

                min_len = 10
                solved = true
                
                for x in [0..8]
                        for y in [0..8]
                                if @get(x,y) is 0
                                        solved = false
                                        p = @possibles(x,y)
                                        if 0 < p.length < min_len
                                                possible = {x,y,p}
                                                min_len = p.length
                                                if min_len == 1
                                                        break
                        if min_len == 1
                                break
                if solved
                        alert("done in #{tick} steps")
                        return this

                unless possible?
                        return false

                tick++
                if tick % 1 == 0
                         @render()
                         #console.log({tick, depth: @depth, possible})
                         await sleep(10)

                # On trouve la première bonne solution
                for child in possible.p
                        new_sudoku = SudokuFactory.fromgrid(this, @depth + 1)
                        new_sudoku.set(possible.x, possible.y, child)
                        res = await new_sudoku.solve()
                        if res isnt false
                                return res
                return false
                

class SudokuFactory
        
        @fromtable: (gridid) ->
                stat = Array(9*9)
                table = document.getElementById(gridid)
                if !table?
                        throw "#{gridid}: pas trouvé"
                sudoku = new SudokuGrid(stat, table, 0)

                for x in [0..8]
                        for y in [0..8]
                                v = sudoku.table.children[y].children[x].children[0].value
                                sudoku.set(x,y,if v isnt "" then Number(v) else 0)
                return sudoku

        @fromgrid: (grid, depth) ->
                sudoku = new SudokuGrid([...grid.status], grid.table, depth)
                return sudoku

test = ->
        g = [
                7, 0, 0, 0, 6, 8, 0, 9, 0,
                0, 1, 2, 4, 0, 0, 0, 0, 0,
                0, 0, 8, 0, 9, 2, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 6, 1, 0,
                0, 0, 7, 0, 0, 0, 8, 0, 0,
                0, 6, 9, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 8, 4, 0, 9, 0, 0,
                0, 0, 0, 0, 0, 5, 1, 7, 0,
                0, 5, 0, 7, 1, 0, 0, 0, 2
        ]
        h = [
                0, 4, 2, 0, 0, 5, 0, 0, 3,
                8, 1, 0, 0, 3, 0, 0, 0, 0,
                0, 0, 0, 2, 0, 0, 0, 0, 0,
                0, 8, 6, 0, 5, 0, 0, 3, 9,
                3, 0, 0, 1, 6, 2, 0, 0, 5,
                1, 5, 0, 0, 9, 0, 6, 2, 0,
                0, 0, 0, 0, 0, 1, 0, 0, 0,
                0, 0, 0, 0, 4, 0, 0, 9, 7,
                5, 0, 0, 8, 0, 0, 2, 4, 0,
        ]
        s = new SudokuGrid(g, document.getElementById("sudoku"), 0)
        s.render()
        res = await s.solve()
        res.render()
